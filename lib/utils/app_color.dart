import 'package:flutter/material.dart';

class AppColor {
  static Color primaryColor = Color(0xFFF9701F);
  static Color secondaryColor = Color(0xFFF9EBE3);
  static Color blackColor = Color(0xFF323334);
  static Color greyColor = Color(0xFF8B8B8C);
  static Color whiteColor = Color(0xFFF9F9F9);

  static Color pageBackgroundColor = Color(0xFFF9F9F9);
}