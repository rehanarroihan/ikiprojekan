class ConstantHelper {
  static final String MAIN_FONT_NAME = 'Poppins';

  static final String PREFS_TOKEN_KEY = 'PREFS_TOKEN_KEY';
  static final String PREFS_IS_USER_LOGGED_IN = 'PREFS_IS_USER_LOGGED_IN';
  static final String PREFS_USER_ROLE = 'PREFS_USER_ROLE';
}