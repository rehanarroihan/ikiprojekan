import 'package:dio/dio.dart';
import 'package:ngamenhub/utils/constant_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class App {
  static App _instance;
  final String apiBaseURL;
  final String appTitle;

  SharedPreferences prefs;
  Dio dio;

  App.configure({
    this.apiBaseURL,
    this.appTitle
  }) {
    _instance = this;
  }

  factory App() {
    if (_instance == null) {
      throw UnimplementedError("App must be configured first.");
    }

    return _instance;
  }

  Future<Null> init() async {
    prefs = await SharedPreferences.getInstance();

    dio = Dio(BaseOptions(
      baseUrl: apiBaseURL,
      connectTimeout: 10000,
      receiveTimeout: 50000,
      responseType: ResponseType.json
    ));

    dio.options.headers = {
      'Authorization': 'Bearer ${prefs.get(ConstantHelper.PREFS_TOKEN_KEY)}'
    };

    dio.interceptors.add(InterceptorsWrapper(onError: (DioError e) async {
      Map<String, dynamic> data = e.response.data;
      if (e.response.statusCode != null) {
        if (e.response.statusCode == 400) {
        }
        // INFO : Kicking out user to login page when !authenticated
        if (e.response.statusCode == 401) {

        }
      }
      return e;
    }));
  }
}