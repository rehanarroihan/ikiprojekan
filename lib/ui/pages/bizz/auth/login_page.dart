import 'package:flutter/material.dart';
import 'package:ngamenhub/ui/pages/bizz/auth/register_page.dart';
import 'package:ngamenhub/ui/widgets/modules/sign_in.dart';
import 'package:ngamenhub/utils/assets_constant_helper.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SignIn(
      logo: AssetsConstantHelper.NGAMEN_HUB_LOGO,
      onSignInSubmit: () {

      },
      onSignUpButtonClicked: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => RegisterPage()
        ));
      },
    );
  }
}
