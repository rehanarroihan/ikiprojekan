import 'package:flutter/material.dart';
import 'package:ngamenhub/ui/pages/bizz/auth/login_page.dart';
import 'package:ngamenhub/ui/widgets/modules/sign_up.dart';
import 'package:ngamenhub/utils/assets_constant_helper.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return SignUp(
      logo: AssetsConstantHelper.NGAMEN_HUB_LOGO,
      onSignUpSubmit: () {

      },
      onSignInButtonClicked: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => LoginPage()
        ));
      },
    );
  }

  void _onSubmit() {

  }
}