import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ngamenhub/cubit/shared/auth_cubit.dart';
import 'package:ngamenhub/ui/pages/bizz/auth/login_page.dart';
import 'package:ngamenhub/ui/pages/bizz/home_page.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  AuthCubit _authCubit;

  @override
  void initState() {
    super.initState();

    _authCubit = BlocProvider.of<AuthCubit>(context);

    _authCubit.isUserAuthenticated();
    test();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: _authCubit,
      listener: (context, state) {
        if (state is UserAuthenticated) {
          Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => HomePage()
          ));
        } else if (state is UserUnauthenticated) {
          print('rene');
          Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => LoginPage()
          ));
        } else{
          print('test');
        }
      },
      child: BlocBuilder(
        cubit: _authCubit,
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text('asu'),
            ),
            body: Container(
              child: FlatButton(
                onPressed: () => _authCubit.test(),
                child: Text('Submit'),
              ),
            ),
          );
        }
      ),
    );
  }

  void test() async {
    await Future.delayed(Duration(seconds: 5));
    print('test');
}
}
