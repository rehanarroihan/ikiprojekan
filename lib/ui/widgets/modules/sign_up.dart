import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ngamenhub/ui/widgets/base/box_input.dart';
import 'package:ngamenhub/ui/widgets/base/button.dart';
import 'package:ngamenhub/utils/app_color.dart';

class SignUp extends StatefulWidget {
  final String logo;
  final Function onSignUpSubmit;
  final Function onSignInButtonClicked;

  SignUp({
    @required this.logo,
    @required this.onSignUpSubmit,
    @required this.onSignInButtonClicked
  });

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.pageBackgroundColor,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.05
                  ),
                  child: Image(
                    image: AssetImage(widget.logo),
                    height: 205,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      BoxInput(
                        controller: _nameController,
                        keyboardType: TextInputType.name,
                        icon: SvgPicture.asset(
                          "assets/icons/auth-person.svg",
                        ),
                        placeholder: "Name",
                      ),
                      SizedBox(height: 16),
                      BoxInput(
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        icon: SvgPicture.asset(
                          "assets/icons/auth-email.svg",
                        ),
                        placeholder: "Email",
                      ),
                      SizedBox(height: 16),
                      BoxInput(
                        controller: _phoneController,
                        keyboardType: TextInputType.name,
                        icon: SvgPicture.asset(
                          "assets/icons/auth-phone.svg",
                        ),
                        placeholder: "Phone Number",
                      ),
                      SizedBox(height: 16),
                      BoxInput(
                        controller: _passwordController,
                        obscureText: true,
                        icon: SvgPicture.asset(
                          "assets/icons/auth-password.svg",
                        ),
                        placeholder: "Password",
                      ),
                      SizedBox(height: 16),
                      Container(
                        width: double.infinity,
                        child: Button(
                          style: AppButtonStyle.primary,
                          text: 'Sign Up',
                          onPressed: () {}
                        ),
                      ),
                      SizedBox(height: 16),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Already have an account? ',
                            style: TextStyle(
                              fontSize: 16,
                              color: AppColor.greyColor,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(width: 4),
                          GestureDetector(
                            onTap: () => widget.onSignInButtonClicked,
                            child: Text(
                              'Sign In',
                              style: TextStyle(
                                fontSize: 16,
                                color: AppColor.primaryColor,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
