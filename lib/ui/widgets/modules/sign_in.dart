import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ngamenhub/ui/widgets/base/box_input.dart';
import 'package:ngamenhub/ui/widgets/base/button.dart';
import 'package:ngamenhub/utils/app_color.dart';

class SignIn extends StatefulWidget {
  final String logo;
  final Function onSignInSubmit;
  final Function onSignUpButtonClicked;

  SignIn({
    this.logo,
    this.onSignInSubmit,
    this.onSignUpButtonClicked
  });

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.pageBackgroundColor,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.07
                  ),
                  child: Image(
                    image: AssetImage(widget.logo),
                    height: 205,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      BoxInput(
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        icon: SvgPicture.asset(
                          "assets/icons/auth-email.svg",
                        ),
                        placeholder: "Email",
                      ),
                      SizedBox(height: 16),
                      BoxInput(
                        controller: _passwordController,
                        obscureText: true,
                        icon: SvgPicture.asset(
                          "assets/icons/auth-password.svg",
                        ),
                        placeholder: "Password",
                      ),
                      SizedBox(height: 16),
                      Container(
                        width: double.infinity,
                        child: Button(
                          style: AppButtonStyle.primary,
                          text: 'Sign In',
                          onPressed: () => widget.onSignInSubmit
                        ),
                      ),
                      SizedBox(height: 16),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Don't have an account?",
                            style: TextStyle(
                              fontSize: 16,
                              color: AppColor.greyColor,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(width: 4),
                          GestureDetector(
                            onTap: () => widget.onSignUpButtonClicked,
                            child: Text(
                              'Sign Up',
                              style: TextStyle(
                                fontSize: 16,
                                color: AppColor.primaryColor,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
