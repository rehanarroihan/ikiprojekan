import 'package:flutter/material.dart';
import 'package:ngamenhub/utils/app_color.dart';

enum AppButtonStyle {
  primary,
  secondary
}

class Button extends StatelessWidget {
  final AppButtonStyle style;
  final String text;
  final bool isLoading;
  final bool isDisabled;
  final Function onPressed;
  final double fontSize;
  final FontWeight fontWeight;

  Button({
    this.style = AppButtonStyle.primary,
    @required this.text,
    @required this.onPressed,
    this.isLoading = false,
    this.isDisabled = false,
    this.fontSize = 18,
    this.fontWeight = FontWeight.w600
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow (
            color: const Color(0x4Df9701f),
            offset: Offset(0, 7),
            blurRadius: 24,
          ),
        ]
      ),
      child: RaisedButton(
        onPressed: !isDisabled ? onPressed : null,
        color: _getButtonColorByStyle(this.style),
        elevation: 0,
        focusElevation: 0,
        highlightElevation: 0,
        hoverElevation: 0,
        padding: EdgeInsets.symmetric(vertical: 18),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        child: Text(
          text,
          style: TextStyle(
            fontSize: fontSize,
            color: _getFontColorByStyle(style),
            fontWeight: fontWeight
          ),
        )
      ),
    );
  }

  Color _getButtonColorByStyle(AppButtonStyle style) {
    Color color;

    switch (style) {
      case AppButtonStyle.primary:
        color = AppColor.primaryColor;
        break;

      case AppButtonStyle.secondary:
        color = AppColor.secondaryColor;
        break;
    }

    return color;
  }

  Color _getFontColorByStyle(AppButtonStyle style) {
    Color color;

    switch (style) {
      case AppButtonStyle.primary:
        color = Colors.white;
        break;

      case AppButtonStyle.secondary:
        color = AppColor.primaryColor;
        break;
    }

    return color;
  }
}
