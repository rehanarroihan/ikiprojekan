import 'package:flutter/material.dart';
import 'package:ngamenhub/utils/app_color.dart';

class BoxInput extends StatelessWidget {
  final Widget icon;
  final String placeholder;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final bool obscureText;

  BoxInput({
    @required this.controller,
    this.icon,
    this.placeholder,
    this.keyboardType = TextInputType.text,
    this.obscureText = false
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Color(0x0Df9701f),
            offset: const Offset(0, 10),
            blurRadius: 20,
          ),
        ],
      ),
      child: TextFormField(
        obscureText: obscureText,
        keyboardType: keyboardType,
        controller: controller,
        decoration: InputDecoration(
          prefixIcon: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: icon,
          ),
          hintText: placeholder,
          hintStyle: TextStyle(
            fontSize: 16,
            color: AppColor.greyColor
          ),
          contentPadding: EdgeInsets.only(
            top: 20, bottom: 24, left: 24
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent, width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          filled: true,
          fillColor: Colors.white,
          errorBorder: InputBorder.none,
        ),
      ),
    );
  }
}
