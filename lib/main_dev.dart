import 'package:flutter/material.dart';
import 'package:ngamenhub/app.dart';
import 'package:ngamenhub/main.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  App.configure(
    apiBaseURL: 'http://192.168.1.14:8000/',
    appTitle: 'NgamenHub Bizz'
  );

  await App().init();

  runApp(MyApp());
}