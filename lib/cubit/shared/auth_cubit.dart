import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ngamenhub/app.dart';
import 'package:ngamenhub/utils/constant_helper.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  void isUserAuthenticated() async {
    emit(HalloInit());
    // await Future.delayed(Duration(seconds: 3));
    bool isLoggedIn = App().prefs.getBool(ConstantHelper.PREFS_IS_USER_LOGGED_IN) ?? false;
    print('Halo: ' + isLoggedIn.toString());
    emit(UserAuthenticated());
  }

  void test() async {
    emit(HalloInit());
    await Future.delayed(Duration(seconds: 3));
    emit(Hallo());
  }
}
