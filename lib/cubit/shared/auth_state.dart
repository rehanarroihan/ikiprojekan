part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class UserUnauthenticated extends AuthState {
  final String message;

  UserUnauthenticated({this.message});

  @override
  List<Object> get props => [message];
}

class UserAuthenticated extends AuthState {
  final String message;

  UserAuthenticated({this.message});

  @override
  List<Object> get props => [message];
}

class HalloInit extends AuthState {
  final String message;

  HalloInit({this.message});

  @override
  List<Object> get props => [message];
}

class Hallo extends AuthState {
  final String message;

  Hallo({this.message});

  @override
  List<Object> get props => [message];
}
