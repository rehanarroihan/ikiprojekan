import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ngamenhub/app.dart';
import 'package:ngamenhub/cubit/shared/auth_cubit.dart';
import 'package:ngamenhub/ui/pages/bizz/home/main_page.dart';
import 'package:ngamenhub/ui/widgets/modules/sign_up.dart';
import 'package:ngamenhub/utils/constant_helper.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthCubit _authCubit = AuthCubit();

    return MaterialApp(
      title: App().appTitle,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: ConstantHelper.MAIN_FONT_NAME
      ),
      home: MultiBlocProvider(
          providers: [
            BlocProvider(create: (_) => _authCubit),
          ],

        child: MainPage()),
    );
  }
}
